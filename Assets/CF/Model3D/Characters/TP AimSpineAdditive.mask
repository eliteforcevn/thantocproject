%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: TP AimSpineAdditive
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: M-motion
    m_Weight: 0
  - m_Path: M-motion/Scene Root
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone L Thigh
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone L Thigh/M-bone L Calf
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone L Thigh/M-bone L Calf/M-bone
      L Foot
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone L Thigh/M-bone L Calf/M-bone
      L Foot/M-bone L Toe0
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone R Thigh
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone R Thigh/M-bone R Calf
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone R Thigh/M-bone R Calf/M-bone
      R Foot
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone R Thigh/M-bone R Calf/M-bone
      R Foot/M-bone R Toe0
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine
    m_Weight: 1
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1
    m_Weight: 1
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck
    m_Weight: 1
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone Head
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/j_elbow_le
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/j_wristfronttwist1_le
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/j_wrist_le
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/j_wrist_le/j_metaindex_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/j_wrist_le/j_metamid_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/j_wrist_le/j_metapinky_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/j_wrist_le/j_metaring_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger0
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger0/j_thumb_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger0/ValveBiped.Bip01_L_Finger01
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger0/ValveBiped.Bip01_L_Finger01/j_thumb_le_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger0/ValveBiped.Bip01_L_Finger01/ValveBiped.Bip01_L_Finger02
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger0/ValveBiped.Bip01_L_Finger01/ValveBiped.Bip01_L_Finger02/j_thumb_le_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger0/ValveBiped.Bip01_L_Finger01/ValveBiped.Bip01_L_Finger02/j_thumb_le_3/j_thumb_le_4
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger1/j_index_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger1/ValveBiped.Bip01_L_Finger11
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger1/ValveBiped.Bip01_L_Finger11/j_index_le_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger1/ValveBiped.Bip01_L_Finger11/ValveBiped.Bip01_L_Finger12
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger1/ValveBiped.Bip01_L_Finger11/ValveBiped.Bip01_L_Finger12/j_index_le_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger1/ValveBiped.Bip01_L_Finger11/ValveBiped.Bip01_L_Finger12/j_index_le_3/j_index_le_4
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger2/j_mid_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger2/ValveBiped.Bip01_L_Finger21
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger2/ValveBiped.Bip01_L_Finger21/j_mid_le_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger2/ValveBiped.Bip01_L_Finger21/ValveBiped.Bip01_L_Finger22
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger2/ValveBiped.Bip01_L_Finger21/ValveBiped.Bip01_L_Finger22/j_mid_le_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger3/j_ring_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger3/ValveBiped.Bip01_L_Finger31
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger3/ValveBiped.Bip01_L_Finger31/j_ring_le_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger3/ValveBiped.Bip01_L_Finger31/ValveBiped.Bip01_L_Finger32
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger3/ValveBiped.Bip01_L_Finger31/ValveBiped.Bip01_L_Finger32/j_ring_le_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger4
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger4/j_pinky_le_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger4/ValveBiped.Bip01_L_Finger41
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger4/ValveBiped.Bip01_L_Finger41/j_pinky_le_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger4/ValveBiped.Bip01_L_Finger41/ValveBiped.Bip01_L_Finger42
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Hand/ValveBiped.Bip01_L_Finger4/ValveBiped.Bip01_L_Finger41/ValveBiped.Bip01_L_Finger42/j_pinky_le_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Ulna
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/M-bone L Forearm/ValveBiped.Bip01_L_Forearm/ValveBiped.Bip01_L_Wrist
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/ValveBiped.Bip01_L_UpperArm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/ValveBiped.Bip01_L_UpperArm/j_shoulder_le
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/ValveBiped.Bip01_L_UpperArm/j_shoulder_le/j_elbowhelperdriver_le
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/M-bone L UpperArm/ValveBiped.Bip01_L_UpperArm/j_shoulder_le/j_shouldertwist_le
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/ValveBiped.Bip01_L_Clavicle
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone L Clavicle/ValveBiped.Bip01_L_Clavicle/j_clavicle_le
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/j_elbow_ri
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/j_wristfronttwist1_ri
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/j_wrist_ri
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/j_wrist_ri/j_metaindex_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/j_wrist_ri/j_metamid_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/j_wrist_ri/j_metapinky_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/j_wrist_ri/j_metaring_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger0
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger0/j_thumb_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger0/ValveBiped.Bip01_R_Finger01
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger0/ValveBiped.Bip01_R_Finger01/j_thumb_ri_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger0/ValveBiped.Bip01_R_Finger01/ValveBiped.Bip01_R_Finger02
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger0/ValveBiped.Bip01_R_Finger01/ValveBiped.Bip01_R_Finger02/j_thumb_ri_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger0/ValveBiped.Bip01_R_Finger01/ValveBiped.Bip01_R_Finger02/j_thumb_ri_3/j_thumb_ri_4
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger1/j_index_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger1/ValveBiped.Bip01_R_Finger11
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger1/ValveBiped.Bip01_R_Finger11/j_index_ri_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger1/ValveBiped.Bip01_R_Finger11/ValveBiped.Bip01_R_Finger12
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger1/ValveBiped.Bip01_R_Finger11/ValveBiped.Bip01_R_Finger12/j_index_ri_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger1/ValveBiped.Bip01_R_Finger11/ValveBiped.Bip01_R_Finger12/j_index_ri_3/j_index_ri_4
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger2/j_mid_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger2/ValveBiped.Bip01_R_Finger21
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger2/ValveBiped.Bip01_R_Finger21/j_mid_ri_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger2/ValveBiped.Bip01_R_Finger21/ValveBiped.Bip01_R_Finger22
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger2/ValveBiped.Bip01_R_Finger21/ValveBiped.Bip01_R_Finger22/j_mid_ri_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger3/j_ring_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger3/ValveBiped.Bip01_R_Finger31
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger3/ValveBiped.Bip01_R_Finger31/j_ring_ri_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger3/ValveBiped.Bip01_R_Finger31/ValveBiped.Bip01_R_Finger32
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger3/ValveBiped.Bip01_R_Finger31/ValveBiped.Bip01_R_Finger32/j_ring_ri_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger4
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger4/j_pinky_ri_1
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger4/ValveBiped.Bip01_R_Finger41
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger4/ValveBiped.Bip01_R_Finger41/j_pinky_ri_2
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger4/ValveBiped.Bip01_R_Finger41/ValveBiped.Bip01_R_Finger42
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Hand/ValveBiped.Bip01_R_Finger4/ValveBiped.Bip01_R_Finger41/ValveBiped.Bip01_R_Finger42/j_pinky_ri_3
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Ulna
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/M-bone R Forearm/ValveBiped.Bip01_R_Forearm/ValveBiped.Bip01_R_Wrist
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/ValveBiped.Bip01_R_UpperArm
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/ValveBiped.Bip01_R_UpperArm/j_shoulder_ri
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/ValveBiped.Bip01_R_UpperArm/j_shoulder_ri/j_elbowhelperdriver_ri
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/M-bone R UpperArm/ValveBiped.Bip01_R_UpperArm/j_shoulder_ri/j_shouldertwist_ri
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/ValveBiped.Bip01_R_Clavicle
    m_Weight: 0
  - m_Path: M-motion/Scene Root/M-bone/M-bone Pelvis/M-bone Spine/M-bone Spine1/M-bone
      Neck/M-bone R Clavicle/ValveBiped.Bip01_R_Clavicle/j_clavicle_ri
    m_Weight: 0
  - m_Path: MW2019_sf_gloves
    m_Weight: 0
  - m_Path: Navyseal_BL_Base_Arms_Gloves
    m_Weight: 0
  - m_Path: Navyseal_BL_Body
    m_Weight: 0
  - m_Path: Navyseal_BL_Face
    m_Weight: 0
