using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP_Wep_Melee : MonoBehaviour
{
    public TP_Wep tP_Wep;
    public string mainDeployName;
    public string mainFire0StateName;
    public string mainFire1StateName;

    public Animator meleeAnimator;

    public bool deploy = false;
    public string deployAnimName;

    public bool fire0 = false;
    public string fire0StateName;

    public bool fire1 = false;
    public string fire1StateName;

    // Start is called before the first frame update
    void Start()
    {
        //animator.GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        _Fire();

        _Deploy();
    }

    private void LateUpdate()
    {
        //float value = animator.GetFloat("Value");

        //Debug.Log("Value = " + value);
    }

    private void OnValidate()
    {
        if(meleeAnimator == null)
        {
            meleeAnimator = GetComponent<Animator>();
        }

        if(tP_Wep == null)
        {
            tP_Wep = transform.parent.GetComponent<TP_Wep>();
        }
    }

    void _Deploy()
    {
        if (deploy == true)
        {
            deploy = false;

            if (mainDeployName != "")
                tP_Wep.characterAnimator.Play(mainDeployName);

            if (deployAnimName != "")
                meleeAnimator.Play(deployAnimName);
        }
    }

    void _Fire()
    {
        if(fire0 == true)
        {
            fire0 = false;

            if (mainFire0StateName != "")
                tP_Wep.characterAnimator.Play(mainFire0StateName);

            if (fire0StateName != "")
                meleeAnimator.Play(fire0StateName);

            return;
        }

        if (fire1 == true)
        {
            fire1 = false;

            if (mainFire1StateName != "")
                tP_Wep.characterAnimator.Play(mainFire1StateName);

            if (fire1StateName != "")
                meleeAnimator.Play(fire1StateName);

            return;
        }
    }
}
