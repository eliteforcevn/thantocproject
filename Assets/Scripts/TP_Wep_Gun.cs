using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP_Wep_Gun : MonoBehaviour
{
    public TP_Wep tP_Wep;

    public int wepType = 0;
    /* * 
    0 = melee
    1 = pistol
    2 = smg
    3 = rifle
    4 = sniper
    5 = grenade
    6 = c4
     * */

    public float aimDegree = 0f;

    public string mainDeploy0Name;
    public string mainFire0StateName;
    public string mainFire1StateName;
    public string mainReload0StateName;
    public string mainReload1StateName;

    public Animator gunAnimator;

    public bool deploy0 = false;
    public string deploy0AnimName;

    public bool fire0 = false;
    public string fire0StateName;

    public bool fire1 = false;
    public string fire1StateName;

    public bool reload0 = false;
    public string reload0StateName;

    public bool reload1 = false;
    public string reload1StateName;

    // Start is called before the first frame update
    void Start()
    {
        //animator.GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        _SetCharacterWepType();

        _SetCharacterAimDegree();

        _MouseFire();

        _Fire();

        _Deploy();

        _Reload();
    }

    private void LateUpdate()
    {
        //float value = animator.GetFloat("Value");

        //Debug.Log("Value = " + value);
    }

    private void OnValidate()
    {
        if (gunAnimator == null)
        {
            gunAnimator = GetComponent<Animator>();
        }

        if (tP_Wep == null)
        {
            tP_Wep = transform.parent.GetComponent<TP_Wep>();
        }
    }

    void _SetCharacterWepType()
    {
        tP_Wep.characterAnimator.SetInteger("WepType", wepType);
    }

    void _SetCharacterAimDegree()
    {
        tP_Wep.characterAnimator.SetFloat("AimDegree", aimDegree);
    }

    void _Deploy()
    {
        if (deploy0 == true)
        {
            deploy0 = false;

            if (mainDeploy0Name != "")
                tP_Wep.characterAnimator.Play(mainDeploy0Name);

            if (deploy0AnimName != "")
                gunAnimator.Play(deploy0AnimName);
        }
    }


    float fireDelay = 0.125f;
    float curFireDelay = 0f;
    void _MouseFire()
    {
        if (Input.GetMouseButtonDown(0))
        {
            fire0 = true;
        }

        if (Input.GetMouseButton(0))
        {
            if (curFireDelay >= 0f)
            {
                curFireDelay -= Time.deltaTime;
            }

            if (curFireDelay < 0f)
            {
                curFireDelay = fireDelay;

                fire0 = true;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            curFireDelay = 0f;

            fire0 = false;
        }
    }

    void _Fire()
    {
        if (fire0 == true)
        {
            fire0 = false;

            if (mainFire0StateName != "")
                tP_Wep.characterAnimator.Play(mainFire0StateName, -1, 0f);

            if (fire0StateName != "")
                gunAnimator.Play(fire0StateName, -1, 0f);

            return;
        }

        if (fire1 == true)
        {
            fire1 = false;

            if (mainFire1StateName != "")
                tP_Wep.characterAnimator.Play(mainFire1StateName, -1, 0f);

            if (fire1StateName != "")
                gunAnimator.Play(fire1StateName, -1, 0f);

            return;
        }
    }

    void _Reload()
    {
        if (reload0 == true)
        {
            reload0 = false;

            if (mainReload0StateName != "")
                tP_Wep.characterAnimator.Play(mainReload0StateName);

            if (reload0StateName != "")
                gunAnimator.Play(reload0StateName);

            return;
        }

        if (reload1 == true)
        {
            reload1 = false;

            if (mainReload1StateName != "")
                tP_Wep.characterAnimator.Play(mainReload1StateName);

            if (reload1StateName != "")
                gunAnimator.Play(reload1StateName);

            return;
        }
    }
}
