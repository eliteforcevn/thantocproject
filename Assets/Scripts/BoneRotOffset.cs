using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneRotOffset : MonoBehaviour
{
    [System.Serializable]
    public class BoneInfo
    {
        public Transform from;
        public Transform to;

        public float lerpX = 1f;
        public float lerpY = 1f;
        public float lerpZ = 1f;

        //public float lerp = 1f;
    }

    public List<BoneInfo> boneInfos = new List<BoneInfo>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        foreach (BoneInfo info in boneInfos)
        {
            float xVal = Mathf.Lerp(info.from.eulerAngles.x, info.to.eulerAngles.x, info.lerpX);
            float yVal = Mathf.Lerp(info.from.eulerAngles.y - 360, info.to.eulerAngles.y - 360, info.lerpY);
            float zVal = Mathf.Lerp(info.from.eulerAngles.z, info.to.eulerAngles.z, info.lerpZ);

            info.from.eulerAngles = new Vector3(xVal, yVal, zVal);

            //info.from.eulerAngles = Vector3.Lerp(info.from.eulerAngles, info.to.eulerAngles, info.lerp);
        }
    }
}
