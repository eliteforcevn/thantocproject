using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType { Knife, Pistol, Rifle, SubMachine, ShotGun, SniperRifle, Grenade, C4}
public enum FireMode { semi, auto}

[System.Serializable]
public class WeaponInfo
{
    public WeaponType weaponType = WeaponType.Knife;

    public FireMode fireMode = FireMode.semi;

    public int reloadType = 0;

    public int fireType = 0;
}
