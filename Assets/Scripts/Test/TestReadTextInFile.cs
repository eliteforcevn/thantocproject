using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System;
using UnityEngine;

public class TestReadTextInFile : MonoBehaviour
{
    public bool getFiles = false;

    private void Start()
    {

    }

    private void Update()
    {
        if (getFiles)
        {
            getFiles = false;

            _Load();
        }
    }

    void _Load()
    {
        string sourcePath = @"F:\Program Files (x86)\CFHD(2000797)\Cross\Content\Rename";

        DirectoryInfo d = new DirectoryInfo(sourcePath); //Assuming Test is your Folder

        FileInfo[] Files = d.GetFiles("*.upk"); //Getting Text files

        string characterPath = @"F:\Program Files (x86)\CFHD(2000797)\Cross\Content\Rename\Characters";

        string[] characterIncludes = new[] { "SK_", "3PC_" };

        string charNameKey = "3PC";

        string weaponPath = @"F:\Program Files (x86)\CFHD(2000797)\Cross\Content\Rename\Weapons";

        string[] weaponIncludes = new[] { "FPS", "_1WP_" };

        string wepNameKey = "_1WP_";

        string removePath = @"F:\Program Files (x86)\CFHD(2000797)\Cross\Content\Rename\Remove";

        foreach (FileInfo file in Files)
        {
            //string str = System.Text.Encoding.Default.GetString(File.ReadAllBytes(file.FullName));

            string str = File.ReadAllText(file.FullName).Replace("\0", _GetNullStr());

            str = _Encode(str);

            Debug.Log("file name = " + file.Name);

            //Check character
            if (_CheckInclude(str, characterIncludes) && _CheckInclude(str, weaponIncludes) == false && str.Contains("Building") == false && str.Contains("Prop") == false)
            {
                Debug.Log("character");

                string trueName = _GetTrueName(str, charNameKey);

                if (trueName.Contains(" "))
                {
                    File.Move(file.FullName, removePath + @"\" + file.Name);

                    continue;
                }

                trueName = _RemoveIllegalCharacters(trueName);

                string movePath = characterPath + @"\" + trueName + file.Extension;

                if (File.Exists(movePath))
                    File.Delete(movePath);

                File.Move(file.FullName, movePath);
                //File.Move(file.FullName, characterPath + @"\" + file.Name);
            }
            else
            //Check weapon
            if (_CheckInclude(str, weaponIncludes))
            {
                Debug.Log("weapon");

                string trueName = _GetTrueName(str, wepNameKey);

                if (trueName.Contains(" "))
                {
                    File.Move(file.FullName, removePath + @"\" + file.Name);

                    continue;
                }

                trueName = _RemoveIllegalCharacters(trueName);

                string movePath = weaponPath + @"\" + trueName + file.Extension;

                if (File.Exists(movePath))
                    File.Delete(movePath);

                File.Move(file.FullName, movePath);
                //File.Move(file.FullName, weaponPath + @"\" + file.Name);
            }
        }
    }

    string _GetTrueName(string Str, string nameKey)
    {
        string nullStr = _GetNullStr();

        string trueStr = nameKey;

        int startKeyIndex = Str.IndexOf(nameKey);
        int endKeyIndex = Str.IndexOf(nameKey) + nameKey.Length - 1;

        int frontIndex = 0;

        while (trueStr.Length < nullStr.Length || trueStr.Substring(0, nullStr.Length) != nullStr)
        {
            frontIndex++;

            trueStr = Str[startKeyIndex - frontIndex] + trueStr;
        }

        int backIndex = 0;

        while (trueStr.Length < nullStr.Length || trueStr.Substring(trueStr.Length - nullStr.Length, nullStr.Length) != nullStr)
        {
            backIndex++;

            trueStr = trueStr + Str[endKeyIndex + backIndex];
        }

        trueStr = trueStr.Replace(nullStr, "").Trim();

        //Debug.Log("startKeyIndex = " + startKeyIndex);
        //Debug.Log("endKeyIndex = " + endKeyIndex);
        Debug.Log("trueName = " + trueStr);

        return trueStr;
    }

    bool _CheckInclude(string checkString, string[] includes)
    {
        foreach(string str in includes)
        {
            if(checkString.Contains(str) == false)
            {
                return false;
            }
        }

        return true;
    }

    string _GetNullStr()
    {
        return "None";
    }

    string _Encode(string inputStr)
    {
        return Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(string.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(inputStr)));
    }

    string _RemoveIllegalCharacters(string inputStr)
    {
        return Regex.Replace(inputStr, "[^a-zA-Z0-9_]+", "_", RegexOptions.Compiled);
    }

    //public static Byte[] ReadAllBytes(this FileInfo @this)
    //{
    //    return File.ReadAllBytes(@this.FullName);
    //}
}
