using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public Animator animator;
    public string mainFireStateName;

    public Animator gunAnimator;
    public string gunFireStateName;

    // Start is called before the first frame update
    void Start()
    {
        //animator.GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        _Fire();
    }

    private void LateUpdate()
    {
        //float value = animator.GetFloat("Value");

        //Debug.Log("Value = " + value);
    }

    public bool fire = false;

    void _Fire()
    {
        if (fire == false)
            return;

        fire = false;

        animator.Play(mainFireStateName);
        gunAnimator.Play(gunFireStateName);
    }
}
