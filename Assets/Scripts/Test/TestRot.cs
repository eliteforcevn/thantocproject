using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRot : MonoBehaviour
{
    Quaternion lastQuaternion;

    public Vector3 euleeAngle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.eulerAngles = euleeAngle;

        if (transform.rotation != lastQuaternion)
        {
            lastQuaternion = transform.rotation;

            Debug.Log("transform.eulerAngles = " + transform.eulerAngles);

            Debug.Log("transform.rotation = " + transform.rotation);

            Debug.Log("transform.rotation.ToEuler = " + transform.rotation.ToEuler());
        }
    }
}
