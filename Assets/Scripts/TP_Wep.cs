using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP_Wep : MonoBehaviour
{
    public Animator characterAnimator;

    public Transform wep_LHand, wep_RHand;

    //public Transform lPole, rPole;
    //public Vector3 lPoleOffset, rPoleOffset;

    //public Transform lLowerArm, rLowerArm;

    [System.Serializable]
    public class BoneFollowInfo
    {
        public Transform target;
        public Transform follow;

        public bool pos = false;
        public bool rot = false;
    }

    public List<BoneFollowInfo> leftHandList = new List<BoneFollowInfo>();
    public List<BoneFollowInfo> rightHandList = new List<BoneFollowInfo>();
    public List<BoneFollowInfo> others = new List<BoneFollowInfo>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //_UpdatePos();

        //_UpdateFollowList();
    }

    private void LateUpdate()
    {
        _UpdateFollowList();
    }

    private void OnValidate()
    {
        _GetHandBones();

        _GetFollowerBones();
    }

    public bool getHandBones = false;

    public bool getBone = false;

    void _GetFollowerBones()
    {
        if (getBone == false)
            return;

        getBone = false;

        List<BoneFollowInfo> cantFoundList = new List<BoneFollowInfo>();

        foreach (BoneFollowInfo b in others)
        {
            if (b.follow == null || b.target != null && b.follow.name.ToLower() != b.target.name.ToLower())
            {
                b.follow = _GetFollower(b.target);
            }

            if(b.follow == null)
            {
                cantFoundList.Add(b);
            }
        }

        //Clean not found bones
        foreach (BoneFollowInfo b in cantFoundList)
        {
            others.Remove(b);
        }
    }

    Transform _GetFollower(Transform target)
    {
        Transform[] ts = characterAnimator.transform.GetComponentsInChildren<Transform>();

        foreach (Transform t in ts)
        {
            if (t.name.ToLower() == target.name.ToLower())
            {
                return t;
            }
        }

        return null;
    }

    void _UpdateFollowList()
    {
        _UpdateFollower(others);

        _UpdateFollower(leftHandList);
        _UpdateFollower(rightHandList);
    }

    void _UpdateFollower(List<BoneFollowInfo> followList)
    {
        foreach (BoneFollowInfo bInfo in followList)
        {
            if (bInfo.follow != null && bInfo.target != null)
            {
                if (bInfo.rot)
                    bInfo.follow.transform.rotation = bInfo.target.transform.rotation;

                if (bInfo.pos)
                    bInfo.follow.transform.position = bInfo.target.transform.position;
            }
        }
    }

    void _GetHandBones()
    {
        if (getHandBones == false)
            return;

        getHandBones = false;

        Transform[] ts = wep_LHand.transform.GetComponentsInChildren<Transform>();

        foreach (Transform t in ts)
        {
            if(wep_LHand != t)
            {
                BoneFollowInfo info = new BoneFollowInfo();

                info.target = t;
                info.pos = false;
                info.rot = true;

                leftHandList.Add(info);
            }
        }

        ts = wep_RHand.transform.GetComponentsInChildren<Transform>();

        foreach (Transform t in ts)
        {
            if (wep_RHand != t)
            {
                BoneFollowInfo info = new BoneFollowInfo();

                info.target = t;
                info.pos = false;
                info.rot = true;

                rightHandList.Add(info);
            }
        }

        List<BoneFollowInfo> cantFoundList = new List<BoneFollowInfo>();

        foreach (BoneFollowInfo b in leftHandList)
        {
            if (b.follow == null || b.target != null && b.follow.name.ToLower() != b.target.name.ToLower())
            {
                b.follow = _GetFollower(b.target);
            }

            if (b.follow == null)
            {
                cantFoundList.Add(b);
            }
        }

        //Clean not found bones
        foreach (BoneFollowInfo b in cantFoundList)
        {
            leftHandList.Remove(b);
        }

        foreach (BoneFollowInfo b in rightHandList)
        {
            if (b.follow == null || b.target != null && b.follow.name.ToLower() != b.target.name.ToLower())
            {
                b.follow = _GetFollower(b.target);
            }

            if (b.follow == null)
            {
                cantFoundList.Add(b);
            }
        }

        //Clean not found bones
        foreach (BoneFollowInfo b in cantFoundList)
        {
            rightHandList.Remove(b);
        }

    }
}
