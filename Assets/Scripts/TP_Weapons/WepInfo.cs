using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Gameplay/WepInfoScriptObj", order = 1)]
public class WepInfo : ScriptableObject
{
    /*
     Base anim type

        melee start = 1
        Pistol start = 2
        Smg start = 3
        rifle = 4
        sniper start = 5
        mg start = 6
        shortgun = 7
        c4 start = 8
        grenade = 9

        -----------------
        anim type id
        knife = 1
        handguns = 2
        mp5 = 3
        m4a1 = 4
        m700 = 5
        m60 = 6
        m136 = 61
        c4 = 8

        wep anim id
        knife = 1
        pistol 1gun = 2
     */

    public string weaponId = "";
    public int animType = 0;

    public float fireRate = 0.1f;
}
