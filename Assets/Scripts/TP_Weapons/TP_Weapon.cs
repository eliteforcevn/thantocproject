using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP_Weapon : MonoBehaviour
{
    [System.Serializable]
    public class FollowBoneInfo
    {
        public Transform follower;
        public Transform target;
    }

    public WepInfo wepInfo;
}
