using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP_PT_Pistol : MonoBehaviour
{
    public TP_Character tP_Character;

    public TP_Weapon tP_Weapon;

    public Animator weaponAnimator;

    string idleStateName = "Handguns Idle";
    string idleClipName = "M-USP_Idle";
    string idleCrouchClipName = "M-c-USP_Idle";
    public AnimationClip idleStandClip;
    public AnimationClip idleCrouchClip;

    string shootStateName = "Handguns Fire";
    string shootClipName = "M-USP_Shoot";
    string shootCrouchClipName = "M-c-USP_Shoot";
    public AnimationClip shootStandClip;
    public AnimationClip shootCrouchClip;
    public int fireFrame = 0;
    public int ejectShellFrame = 0;

    string reloadStateName = "Handguns Reload";
    string reloadClipName = "M-USP_Reload";
    public AnimationClip reloadClip;
    public int dropMagFrame = 0;
    public int showMagFrame = 0;

    bool defaultSet = false;
    float curFireRateCount = 0f;

    public List<TP_Weapon.FollowBoneInfo> followBoneInfos;

    private void OnEnable()
    {
        defaultSet = true;
    }

    private void Update()
    {
        _Default();

        _Input();

        _UpdateStatsFromCharacter();
    }

    private void LateUpdate()
    {
        _UpdateFollowers();
    }

    public void _Default()
    {
        if (defaultSet == false) return;

        defaultSet = false;

        tP_Weapon = GetComponent<TP_Weapon>();

        tP_Character.characterAnimator.SetInteger("AnimType", tP_Weapon.wepInfo.animType);

        curFireRateCount = 0f;

        _SetBoneFollowers();

        _ReplaceAnimations();
    }

    void _Input()
    {
        //Shoot
        if (Input.GetMouseButtonDown(0))
        {
            curFireRateCount = tP_Weapon.wepInfo.fireRate;
            _Shoot();
        }

        if (Input.GetMouseButton(0))
        {
            if(curFireRateCount >= 0f)
            {
                curFireRateCount -= Time.deltaTime;

                if(curFireRateCount < 0f)
                {
                    curFireRateCount = tP_Weapon.wepInfo.fireRate;
                    _Shoot();
                }
            }
        }

        //Reload
        if (Input.GetButtonDown("Reload"))
        {
            Debug.Log("Reload");

            _Reload();
        }
    }

    void _UpdateFollowers()
    {
        foreach (TP_Weapon.FollowBoneInfo info in followBoneInfos)
        {
            info.follower.transform.position = info.target.transform.position;
            info.follower.transform.rotation = info.target.transform.rotation;
        }
    }

    void _SetBoneFollowers()
    {
        Transform[] transforms = tP_Character.characterAnimator.transform.parent.GetComponentsInChildren<Transform>();

        foreach (TP_Weapon.FollowBoneInfo info in followBoneInfos)
        {
            info.target = _GetTargetBone(info, transforms);
        }
    }

    Transform _GetTargetBone(TP_Weapon.FollowBoneInfo info, Transform[] transforms)
    {
        Transform trans = null;

        foreach (Transform t in transforms)
        {
            if (t.name == info.follower.name)
            {
                trans = t;

                break;
            }
        }

        return trans;
    }

    void _ReplaceAnimations()
    {
        AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(weaponAnimator.runtimeAnimatorController);

        animatorOverrideController[idleClipName] = idleStandClip;
        animatorOverrideController[idleCrouchClipName] = idleCrouchClip;

        animatorOverrideController[shootClipName] = shootStandClip;
        animatorOverrideController[shootCrouchClipName] = shootCrouchClip;

        animatorOverrideController[reloadClipName] = reloadClip;

        weaponAnimator.runtimeAnimatorController = animatorOverrideController;
    }

    void _UpdateStatsFromCharacter()
    {
        weaponAnimator.SetBool("Crouch", tP_Character.characterAnimator.GetBool("Crouch"));
        weaponAnimator.SetFloat("CrouchFloat", tP_Character.characterAnimator.GetFloat("CrouchFloat"));
    }

    public void _Idle()
    {
        //AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(weaponAnimator.runtimeAnimatorController);

        //animatorOverrideController[idleClipName] = idleStandClip;
        //animatorOverrideController[idleCrouchClipName] = idleCrouchClip;

        //weaponAnimator.runtimeAnimatorController = animatorOverrideController;

        tP_Character.characterAnimator.Play(idleStateName, -1, 0f);
        weaponAnimator.Play(idleStateName, -1, 0f);
    }

    public void _Shoot()
    {
        //AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(weaponAnimator.runtimeAnimatorController);

        //animatorOverrideController[shootClipName] = shootStandClip;
        //animatorOverrideController[shootCrouchClipName] = shootCrouchClip;

        //weaponAnimator.runtimeAnimatorController = animatorOverrideController;

        tP_Character.characterAnimator.Play(shootStateName, -1, 0f);
        weaponAnimator.Play(shootStateName, -1, 0f);
    }

    public void _Reload()
    {
        //AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(weaponAnimator.runtimeAnimatorController);

        //animatorOverrideController[shootClipName] = shootStandClip;
        //animatorOverrideController[shootCrouchClipName] = shootCrouchClip;

        //weaponAnimator.runtimeAnimatorController = animatorOverrideController;

        tP_Character.characterAnimator.Play(reloadStateName, -1, 0f);
        weaponAnimator.Play(reloadStateName, -1, 0f);
    }
}
