using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixInvisibleSkinedMeshTooCloseCam : MonoBehaviour
{
    public SkinnedMeshRenderer MainMesh;

    public List<SkinnedMeshRenderer> AttachMeshes = new List<SkinnedMeshRenderer>();

    // Start is called before the first frame update
    void Start()
    {
        _GetSkinedMesh();

        _SetRootBone();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {

    }

    void _GetSkinedMesh()
    {
        SkinnedMeshRenderer[] meshes = GetComponentsInChildren<SkinnedMeshRenderer>();

        foreach(SkinnedMeshRenderer m in meshes)
        {
            if(m != MainMesh)
            {
                AttachMeshes.Add(m);
            }
        }
    }

    void _SetRootBone()
    {
        foreach (SkinnedMeshRenderer m in AttachMeshes)
        {
            m.rootBone.transform.position = MainMesh.rootBone.transform.position;
        }
    }
}
