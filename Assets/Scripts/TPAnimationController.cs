using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPAnimationController : MonoBehaviour
{
    public Animator TPCharAnimator;

    public WeaponInfo currentWepInfo;

    public bool select = false;

    public bool stand = false;
    public bool walk = false;
    public bool sit = false;

    [Range(-1f,1f)]
    public float vertical = 0f; //range from -1 to 1;
    [Range(-1f, 1f)]
    public float horizontal = 0f; //range from -1 to 1;
    [Range(-90f, 90f)]
    public float aimDegree = 0f; //range from -90 to 90;

    public const string knifeUpperLayer = "UpperKnife";
    public const string pistolUpperLayer = "UpperPistol";
    public const string rifleUpperLayer = "UpperRifle";
    public const string shotgunUpperLayer = "UpperShotgun";
    public const string sniperUpperLayer = "UpperSniper";
    public const string grenadeUpperLayer = "UpperGrenade";
    public const string c4UpperLayer = "UpperC4";

    public const string wepTypeName = "WepType";
    //WepType 0 == Knife, 1 == Pistol, 2 == Rifle, 3 == Submachine, 4 == Shotgun, 5 == Sniperrifle, 6 == Grenade, 7 == C4

    public const string animFireLoop = "FireLoop";
    public const string animFiring = "Firing";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _UpdateStats();

        _Select();

        _Fire();
    }

    void _UpdateStats()
    {
        _SetLayerWeight();

        TPCharAnimator.SetBool("Stand", stand);
        TPCharAnimator.SetBool("Walk", walk);
        TPCharAnimator.SetBool("Sit", sit);

        TPCharAnimator.SetFloat("Veritical", vertical);
        TPCharAnimator.SetFloat("Horizontal", horizontal);
        TPCharAnimator.SetFloat("AimDegree", aimDegree);
    }

    int _GetAnimLayerIndex(string layerName)
    {
        return TPCharAnimator.GetLayerIndex(layerName);
    }

    void _SetLayerWeight()
    {
        if (currentWepInfo.weaponType == WeaponType.Knife && TPCharAnimator.GetLayerWeight(_GetAnimLayerIndex(knifeUpperLayer)) < 1)
        {
            _SetWeightZero();

            TPCharAnimator.SetInteger(wepTypeName, 0);

            TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(knifeUpperLayer), 1);
        }

        if (currentWepInfo.weaponType == WeaponType.Pistol && TPCharAnimator.GetLayerWeight(_GetAnimLayerIndex(pistolUpperLayer)) < 1)
        {
            _SetWeightZero();

            TPCharAnimator.SetInteger(wepTypeName, 1);

            TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(pistolUpperLayer), 1);
        }

        if (TPCharAnimator.GetLayerWeight(_GetAnimLayerIndex(rifleUpperLayer)) < 1)
        {
            if (currentWepInfo.weaponType == WeaponType.Rifle || currentWepInfo.weaponType == WeaponType.SubMachine)
            {
                _SetWeightZero();

                if (currentWepInfo.weaponType == WeaponType.Rifle)
                    TPCharAnimator.SetInteger(wepTypeName, 2);

                if (currentWepInfo.weaponType == WeaponType.SubMachine)
                    TPCharAnimator.SetInteger(wepTypeName, 3);

                TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(rifleUpperLayer), 1);
            }
        }

        if (currentWepInfo.weaponType == WeaponType.ShotGun && TPCharAnimator.GetLayerWeight(_GetAnimLayerIndex(shotgunUpperLayer)) < 1)
        {
            _SetWeightZero();

            TPCharAnimator.SetInteger(wepTypeName, 4);

            TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(shotgunUpperLayer), 1);
        }

        if (currentWepInfo.weaponType == WeaponType.SniperRifle && TPCharAnimator.GetLayerWeight(_GetAnimLayerIndex(sniperUpperLayer)) < 1)
        {
            _SetWeightZero();

            TPCharAnimator.SetInteger(wepTypeName, 5);

            TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(sniperUpperLayer), 1);
        }

        if (currentWepInfo.weaponType == WeaponType.Grenade && TPCharAnimator.GetLayerWeight(_GetAnimLayerIndex(grenadeUpperLayer)) < 1)
        {
            _SetWeightZero();

            TPCharAnimator.SetInteger(wepTypeName, 6);

            TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(grenadeUpperLayer), 1);
        }

        if (currentWepInfo.weaponType == WeaponType.C4 && TPCharAnimator.GetLayerWeight(_GetAnimLayerIndex(c4UpperLayer)) < 1)
        {
            _SetWeightZero();

            TPCharAnimator.SetInteger(wepTypeName, 7);

            TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(c4UpperLayer), 1);
        }
    }

    void _SetWeightZero()
    {
        TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(knifeUpperLayer), 0);
        TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(pistolUpperLayer), 0);
        TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(rifleUpperLayer), 0);
        TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(shotgunUpperLayer), 0);
        TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(sniperUpperLayer), 0);
        TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(grenadeUpperLayer), 0);
        TPCharAnimator.SetLayerWeight(_GetAnimLayerIndex(c4UpperLayer), 0);
    }

    void _Select()
    {
        if (select == false)
            return;

        select = false;

        _AnimSelectWeapon();
    }

    void _AnimSelectWeapon()
    {
        if(currentWepInfo.weaponType == WeaponType.Knife)
        {
            TPCharAnimator.Play("Select", _GetAnimLayerIndex(knifeUpperLayer));
        }

        if (currentWepInfo.weaponType == WeaponType.Pistol)
        {
            TPCharAnimator.Play("Select", _GetAnimLayerIndex(pistolUpperLayer));
        }

        if (currentWepInfo.weaponType == WeaponType.Rifle || currentWepInfo.weaponType == WeaponType.SubMachine)
        {
            TPCharAnimator.Play("Select", _GetAnimLayerIndex(rifleUpperLayer));
        }

        if (currentWepInfo.weaponType == WeaponType.ShotGun)
        {
            TPCharAnimator.Play("Select", _GetAnimLayerIndex(shotgunUpperLayer));
        }

        if (currentWepInfo.weaponType == WeaponType.SniperRifle)
        {
            TPCharAnimator.Play("Select", _GetAnimLayerIndex(sniperUpperLayer));
        }

        if (currentWepInfo.weaponType == WeaponType.Grenade)
        {
            TPCharAnimator.Play("Select", _GetAnimLayerIndex(grenadeUpperLayer));
        }

        if (currentWepInfo.weaponType == WeaponType.C4)
        {
            TPCharAnimator.Play("Select", _GetAnimLayerIndex(c4UpperLayer));
        }
    }

    void _Fire()
    {
        _KnifeFire();

        _PistolFire();

        _RifleFire();

        _SubmachineFire();

        _ShotgunFire();

        _SniperFire();
    }

    void _KnifeFire()
    {
        if (currentWepInfo.weaponType == WeaponType.Knife)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _AnimKnifeFire(false);
            }

            if (Input.GetMouseButtonDown(1))
            {
                _AnimKnifeFire(true);
            }
        }
    }

    void _AnimKnifeFire(bool specFire = false)
    {
        if (specFire == false)
        {
            switch (currentWepInfo.fireType)
            {
                case 0:
                    TPCharAnimator.Play("Normal Knife", _GetAnimLayerIndex(knifeUpperLayer));
                    break;
                case 1:
                    TPCharAnimator.Play("Normal Gurkha", _GetAnimLayerIndex(knifeUpperLayer));
                    break;
                case 2:
                    TPCharAnimator.Play("Normal Tiger", _GetAnimLayerIndex(knifeUpperLayer));
                    break;
            }
        }
        else
        {
            switch (currentWepInfo.fireType)
            {
                case 0:
                    TPCharAnimator.Play("Spec Knife", _GetAnimLayerIndex(knifeUpperLayer));
                    break;
                case 1:
                    TPCharAnimator.Play("Spec Gurkha", _GetAnimLayerIndex(knifeUpperLayer));
                    break;
                case 2:
                    TPCharAnimator.Play("Spec Tiger", _GetAnimLayerIndex(knifeUpperLayer));
                    break;
            }
        }
    }

    void _PistolFire()
    {
        if (currentWepInfo.weaponType == WeaponType.Pistol)
        {
            if (Input.GetMouseButtonDown(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, true);

                _AnimPistolFire(false);
            }

            if (Input.GetMouseButton(0) && currentWepInfo.fireMode == FireMode.auto)
            {
                _AnimPistolFire(true);
            }

            if (Input.GetMouseButtonUp(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, false);
            }
        }
    }

    void _AnimPistolFire(bool fireLoop = false)
    {
        if(fireLoop)
        {
            TPCharAnimator.SetBool(animFireLoop, true);
        }
        else
        {
            switch (currentWepInfo.fireType)
            {
                case 0:
                    TPCharAnimator.Play("Signle Fire", _GetAnimLayerIndex(pistolUpperLayer));
                    break;
            }
        }
    }

    void _RifleFire()
    {
        if (currentWepInfo.weaponType == WeaponType.Rifle)
        {
            if (Input.GetMouseButtonDown(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, true);

                _AnimRifleFire(false);
            }

            if (Input.GetMouseButton(0) && currentWepInfo.fireMode == FireMode.auto)
            {
                _AnimRifleFire(true);
            }

            if (Input.GetMouseButtonUp(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, false);
            }
        }
    }

    void _AnimRifleFire(bool fireLoop = false)
    {
        if (fireLoop)
        {
            TPCharAnimator.SetBool(animFireLoop, true);
        }
        else
        {
            switch (currentWepInfo.fireType)
            {
                case 0:
                    TPCharAnimator.Play("Signle Fire", _GetAnimLayerIndex(rifleUpperLayer));
                    break;
            }
        }
    }

    void _SubmachineFire()
    {
        if (currentWepInfo.weaponType == WeaponType.SubMachine)
        {
            if (Input.GetMouseButtonDown(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, true);

                _AnimSubmachineFire(false);
            }

            if (Input.GetMouseButton(0) && currentWepInfo.fireMode == FireMode.auto)
            {
                _AnimSubmachineFire(true);
            }

            if (Input.GetMouseButtonUp(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, false);
            }
        }
    }

    void _AnimSubmachineFire(bool fireLoop = false)
    {
        if (fireLoop)
        {
            TPCharAnimator.SetBool(animFireLoop, true);
        }
        else
        {
            switch (currentWepInfo.fireType)
            {
                case 0:
                    TPCharAnimator.Play("Signle Fire", _GetAnimLayerIndex(rifleUpperLayer));
                    break;
            }
        }
    }

    void _ShotgunFire()
    {
        if (currentWepInfo.weaponType == WeaponType.ShotGun)
        {
            if (Input.GetMouseButtonDown(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, true);

                _AnimShotgunFire(false);
            }

            if (Input.GetMouseButton(0) && currentWepInfo.fireMode == FireMode.auto)
            {
                _AnimShotgunFire(true);
            }

            if (Input.GetMouseButtonUp(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, false);
            }
        }
    }

    void _AnimShotgunFire(bool fireLoop = false)
    {
        if (fireLoop)
        {
            TPCharAnimator.SetBool(animFireLoop, true);
        }
        else
        {
            switch (currentWepInfo.fireType)
            {
                case 0:
                    TPCharAnimator.Play("Signle Fire", _GetAnimLayerIndex(shotgunUpperLayer));
                    break;
            }
        }
    }

    void _SniperFire()
    {
        if (currentWepInfo.weaponType == WeaponType.SniperRifle)
        {
            if (Input.GetMouseButtonDown(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, true);

                _AnimSniperFire(false);
            }

            if (Input.GetMouseButton(0) && currentWepInfo.fireMode == FireMode.auto)
            {
                _AnimSniperFire(true);
            }

            if (Input.GetMouseButtonUp(0))
            {
                TPCharAnimator.SetBool(animFireLoop, false);

                TPCharAnimator.SetBool(animFiring, false);
            }
        }
    }

    void _AnimSniperFire(bool fireLoop = false)
    {
        if (fireLoop)
        {
            TPCharAnimator.SetBool(animFireLoop, true);
        }
        else
        {
            switch (currentWepInfo.fireType)
            {
                case 0:
                    TPCharAnimator.Play("Signle Fire", _GetAnimLayerIndex(sniperUpperLayer));
                    break;
                case 1:
                    TPCharAnimator.Play("Single Fire SVD", _GetAnimLayerIndex(sniperUpperLayer));
                    break;
            }
        }
    }
}
