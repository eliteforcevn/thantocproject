using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class TP_Wep_Sniper : MonoBehaviour
{
    public TP_Wep tP_Wep;
    public string mainDeployName;
    public string mainFire0StateName;
    public string mainFire1StateName;
    public string mainReloadStateName;

    public Animator sniperAnimator;

    public bool deploy = false;
    public string deployAnimName;

    public bool fire0 = false;
    public string fire0StateName;

    public bool fire1 = false;
    public string fire1StateName;

    public bool reload = false;
    public string reloadStateName;

    // Start is called before the first frame update
    void Start()
    {
        //animator.GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        _Deploy();

        _Fire();

        _Reload();
    }

    private void LateUpdate()
    {
        //float value = animator.GetFloat("Value");

        //Debug.Log("Value = " + value);
    }

    private void OnValidate()
    {
        if (sniperAnimator == null)
        {
            sniperAnimator = GetComponent<Animator>();
        }

        //if (tP_Wep == null)
        //{
        //    tP_Wep = transform.parent.GetComponent<TP_Wep>();
        //}
    }

    void _Deploy()
    {
        if (deploy == true)
        {
            deploy = false;

            if (tP_Wep != null && mainDeployName != "")
                tP_Wep.characterAnimator.Play(mainDeployName);

            if (deployAnimName != "")
                sniperAnimator.Play(deployAnimName);
        }
    }

    void _Fire()
    {
        if (fire0 == true)
        {
            fire0 = false;

            if (tP_Wep != null && mainFire0StateName != "")
                tP_Wep.characterAnimator.Play(mainFire0StateName);

            if (fire0StateName != "")
                sniperAnimator.Play(fire0StateName);

            return;
        }

        if (fire1 == true)
        {
            fire1 = false;

            if (tP_Wep != null && mainFire1StateName != "")
                tP_Wep.characterAnimator.Play(mainFire1StateName);

            if (fire1StateName != "")
                sniperAnimator.Play(fire1StateName);

            return;
        }
    }

    void _Reload()
    {
        if (reload == true)
        {
            reload = false;

            if (tP_Wep != null && mainReloadStateName != "")
                tP_Wep.characterAnimator.Play(mainReloadStateName);

            if (reloadStateName != "")
                sniperAnimator.Play(reloadStateName);
        }
    }
}
